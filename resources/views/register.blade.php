<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Signup</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="{{ url ('/welcome') }}" method="POST">
        @csrf
        <label>First Name:</label><br><br>
        <input type="text" name="namadepan"><br><br>
        <label>Last Name:</label><br><br>
        <input type="text" name="namabelakang"><br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name="kelamin">Male<br>
        <input type="radio" name="kelamin">Female<br>
        <input type="radio" name="kelamin">Other<br><br>
        <label>Nationality:</label><br><br>
        <select name="kebangsaan">
            <option value="australia">Australian</option>
            <option value="british">British</option>
            <option value="indonesia">Indonesian</option>
            <option value="malaysia">Malaysian</option>
        </select><br><br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox">Bahasa Indonesia<br>
        <input type="checkbox">English<br>
        <input type="checkbox">Other<br><br>
        <label>Bio:</label><br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br><br>
        <input type="submit">
    </form>
</body>
</html>